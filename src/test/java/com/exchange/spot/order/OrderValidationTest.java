package com.exchange.spot.order;

import com.exchange.spot.asset.Asset;
import com.exchange.spot.common.exception.ValidationException;
import com.exchange.spot.order.dto.CreateOrderReq;
import com.exchange.spot.order.entity.OrderDirection;
import com.exchange.spot.order.entity.OrderType;
import com.exchange.spot.order.srevice.OrderParserService;
import com.exchange.spot.order.srevice.limit.LimitOrderParser;
import com.exchange.spot.order.srevice.market.MarketOrderParser;
import com.exchange.spot.pair.Pair;
import com.exchange.spot.pair.PairService;
import com.exchange.spot.user.entity.User;
import com.exchange.spot.user.service.UserService;
import com.exchange.spot.wallet.service.OrderSpendBalanceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mockito;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OrderValidationTest {
    OrderParserService orderParserService;
    MarketOrderParser marketOrderParser;
    LimitOrderParser limitOrderParser;
    OrderSpendBalanceService spendBalanceService;
    PairService pairService;
    UserService userService;

    @BeforeAll
    void init() {
        spendBalanceService = Mockito.mock(OrderSpendBalanceService.class);
        pairService = Mockito.mock(PairService.class);
        userService = Mockito.mock(UserService.class);
        marketOrderParser = new MarketOrderParser();
        limitOrderParser = new LimitOrderParser();
        orderParserService = new OrderParserService(marketOrderParser, limitOrderParser);
        marketOrderParser.setPairService(pairService);
        marketOrderParser.setUserService(userService);
        marketOrderParser.setSpendBalanceService(spendBalanceService);
        limitOrderParser.setPairService(pairService);
        limitOrderParser.setUserService(userService);
        limitOrderParser.setSpendBalanceService(spendBalanceService);
    }

    @ParameterizedTest
    @EnumSource(OrderType.class)
    @DisplayName("Base Order Validation")
    void baseOrderValidation(OrderType type) {
        CreateOrderReq createOrderReq = new CreateOrderReq();
        createOrderReq.setType(type);
        Assertions.assertAll(() -> {
            Assertions.assertThrows(ValidationException.class,
                    () -> orderParserService.parse(createOrderReq)
            );
            createOrderReq.setDirection(OrderDirection.ASK);
            Assertions.assertThrows(ValidationException.class,
                    () -> orderParserService.parse(createOrderReq)
            );
            createOrderReq.setPair("BTCUSDT");
            Assertions.assertThrows(ValidationException.class,
                    () -> orderParserService.parse(createOrderReq)
            );
            createOrderReq.setQuantity(BigDecimal.TEN);
            Assertions.assertThrows(ValidationException.class,
                    () -> orderParserService.parse(createOrderReq)
            );
            createOrderReq.setUser(1L);
            Mockito.when(userService.getById(any())).thenReturn(new User(1L));
            var mockedPair = new Pair();
            mockedPair.setName(createOrderReq.getPair());
            mockedPair.setAsset1(new Asset(1L));
            mockedPair.setAsset2(new Asset(2L));
            Mockito.when(pairService.getByName(any())).thenReturn(mockedPair);
            Mockito.when(spendBalanceService.isValidToSpend(any(), any(), any())).thenReturn(true);
            if (OrderType.LIMIT.equals(type)) {
                createOrderReq.setPrice(BigDecimal.TEN);
            }
            Assertions.assertDoesNotThrow(() -> orderParserService.parse(createOrderReq)
            );
        });
    }
}
