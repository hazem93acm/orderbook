package com.exchange.spot.pair.dto;

import com.exchange.spot.asset.dto.AssetDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PairDto {
    private String name;
    private Long id;
    private AssetDto asset1;
    private AssetDto asset2;
}
