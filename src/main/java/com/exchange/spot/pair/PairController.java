package com.exchange.spot.pair;

import com.exchange.spot.pair.dto.CreatePairReq;
import com.exchange.spot.pair.dto.PairDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/pair")
@RequiredArgsConstructor
public class PairController {
    private final PairService pairService;

    @PostMapping
    public PairDto create(@RequestBody CreatePairReq createPairReq) {
        return
                PairEntityConverter.INST.fromEntity(pairService.create(createPairReq.getAsset1(), createPairReq.getAsset2()));
    }
}
