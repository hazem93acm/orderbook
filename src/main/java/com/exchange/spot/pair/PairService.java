package com.exchange.spot.pair;

import com.exchange.spot.asset.AssetService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PairService {
    private final PairRepository pairRepository;
    private final AssetService assetService;

    @Transactional
    public Pair create(String asset1, String asset2) {
        if (asset1.equals(asset2)) {
            throw new IllegalArgumentException();
        }
        var pair = new Pair();
        pair.setAsset1(assetService.getByName(asset1));
        pair.setAsset2(assetService.getByName(asset2));
        pair.setName(asset1 + asset2);
        pairRepository.save(pair);
        return pair;
    }

    public Pair getByName(String name) {
        return pairRepository.getByName(name).orElseThrow(() -> new EntityNotFoundException("no pair found for the given name"));
    }

}
