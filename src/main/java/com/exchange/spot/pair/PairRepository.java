package com.exchange.spot.pair;

import com.exchange.spot.common.GenericRepository;

import java.util.Optional;

public interface PairRepository extends GenericRepository<Pair, Long> {
    Optional<Pair> getByName(String name);
}
