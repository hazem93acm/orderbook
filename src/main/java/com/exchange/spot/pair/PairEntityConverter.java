package com.exchange.spot.pair;

import com.exchange.spot.asset.Asset;
import com.exchange.spot.asset.dto.AssetDto;
import com.exchange.spot.common.IEntityConverter;
import com.exchange.spot.pair.dto.PairDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PairEntityConverter extends IEntityConverter<PairDto, Pair> {
    PairEntityConverter INST = Mappers.getMapper(PairEntityConverter.class);
}
