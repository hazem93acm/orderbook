package com.exchange.spot.pair;

import com.exchange.spot.asset.Asset;
import com.exchange.spot.common.GenericEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Pair extends GenericEntity<Long> {
    @ManyToOne
    private Asset asset1;
    @ManyToOne
    private Asset asset2;
    private String name;
}
