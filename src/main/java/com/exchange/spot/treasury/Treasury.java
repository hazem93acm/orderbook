package com.exchange.spot.treasury;

import com.exchange.spot.asset.Asset;
import com.exchange.spot.common.GenericEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class Treasury extends GenericEntity<Long> {
    @ManyToOne
    private Asset asset;
    private BigDecimal balance;
}
