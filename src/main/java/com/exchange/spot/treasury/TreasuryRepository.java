package com.exchange.spot.treasury;

import com.exchange.spot.asset.Asset;
import com.exchange.spot.common.GenericRepository;

public interface TreasuryRepository extends GenericRepository<Treasury, Long> {
    Treasury getByAsset(Asset asset);
}
