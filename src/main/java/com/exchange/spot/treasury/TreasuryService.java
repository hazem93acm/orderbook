package com.exchange.spot.treasury;

import com.exchange.spot.asset.Asset;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class TreasuryService {
    private final TreasuryRepository treasuryRepository;

    public Treasury add(Long assetId, BigDecimal amount) {
        var asset = new Asset(assetId);
        var assetTreasury = treasuryRepository.getByAsset(asset);
        if (assetTreasury == null) {
            Treasury treasury = new Treasury();
            treasury.setAsset(asset);
            treasury.setBalance(amount);
            return treasuryRepository.save(treasury);
        }
        assetTreasury.setBalance(assetTreasury.getBalance().add(amount));
        return treasuryRepository.save(assetTreasury);
    }
}
