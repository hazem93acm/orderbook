package com.exchange.spot.fee;

import com.exchange.spot.common.GenericEntity;
import com.exchange.spot.user.entity.User;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class UserFeeEntity extends GenericEntity<Long> {
    @ManyToOne
    @JoinColumn(updatable = false,nullable = false)
    private User user;
    @Column(precision = 7, scale = 4) // 100.0000
    private BigDecimal discountPercentage;
}
