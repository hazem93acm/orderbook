package com.exchange.spot.fee;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
@ConfigurationProperties(prefix = "app.fee.spot")
@Getter
@Setter
public class PlatformFeeConfig {
    private BigDecimal market;
    private BigDecimal limit;
}
