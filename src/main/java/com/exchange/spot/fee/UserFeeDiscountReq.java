package com.exchange.spot.fee;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserFeeDiscountReq {
    private Long userId;
    private BigDecimal discount;
}
