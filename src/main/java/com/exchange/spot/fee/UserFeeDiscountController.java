package com.exchange.spot.fee;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/vi/fee-discount")
@RequiredArgsConstructor
public class UserFeeDiscountController {
    private final FeeService feeService;

    @PostMapping
    public UserFeeEntity create(@RequestBody UserFeeDiscountReq userFeeDiscountReq) {
        return feeService.create(userFeeDiscountReq.getUserId(), userFeeDiscountReq.getDiscount());
    }

    @GetMapping("user/{userId}")
    public List<UserFeeEntity> getUserDiscounts(@PathVariable("userId") Long userId) {
        return feeService.getDiscounts(userId);
    }
}
