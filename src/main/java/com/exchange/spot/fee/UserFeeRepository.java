package com.exchange.spot.fee;

import com.exchange.spot.common.GenericRepository;
import com.exchange.spot.user.entity.User;

import java.util.List;

public interface UserFeeRepository extends GenericRepository<UserFeeEntity,Long> {
    List<UserFeeEntity> getByUser(User user);
}
