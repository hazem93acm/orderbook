package com.exchange.spot.fee;

import com.exchange.spot.order.entity.OrderType;
import com.exchange.spot.user.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FeeService {
    private final UserFeeRepository feeRepository;
    private final PlatformFeeConfig platformFeeConfig;

    public UserFeeEntity create(Long userId, BigDecimal discount) {
        UserFeeEntity feeEntity = new UserFeeEntity();
        feeEntity.setDiscountPercentage(discount);
        feeEntity.setUser(new User(userId));
        return feeRepository.save(feeEntity);
    }

    public List<UserFeeEntity> getDiscounts(Long userId) {
        return feeRepository.getByUser(new User(userId));
    }

    public BigDecimal calculateFee(OrderType type, BigDecimal amount, Long userId) {
        var fee = BigDecimal.ZERO;
        if (OrderType.MARKET.equals(type)) {
            fee = platformFeeConfig.getMarket();
        } else if (OrderType.LIMIT.equals(type)) {
            fee = platformFeeConfig.getLimit();
        }
        if (fee.equals(BigDecimal.ZERO)) {
            return BigDecimal.ZERO;
        }
        var discounts = getDiscounts(userId);
        if (discounts != null && !discounts.isEmpty()) {
            for (UserFeeEntity discount : discounts) {
                fee = fee.subtract(fee.multiply(discount.getDiscountPercentage()).divide(BigDecimal.TEN.pow(2), RoundingMode.HALF_EVEN));
            }
        }
        return amount.multiply(fee);
    }
}
