package com.exchange.spot.order.controller;

import com.exchange.spot.common.PageableResponse;
import com.exchange.spot.order.converter.OrderEntityConverter;
import com.exchange.spot.order.dto.CreateOrderReq;
import com.exchange.spot.order.dto.OrderDto;
import com.exchange.spot.order.entity.Order;
import com.exchange.spot.order.entity.OrderStatus;
import com.exchange.spot.order.srevice.OrderService;
import jakarta.xml.bind.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/spot")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @PostMapping("order")
    public OrderDto createOrder(@RequestBody CreateOrderReq createOrderReq) throws ValidationException, IllegalAccessException {
        return OrderEntityConverter.INS.fromEntity(orderService.createOrder(createOrderReq));
    }

    @GetMapping("order/{id}")
    public OrderDto getOrderById(@PathVariable("id") Long id) {
        return OrderEntityConverter.INS.fromEntity(orderService.getById(id));
    }

    @GetMapping("order/user/{userId}")
    public PageableResponse<OrderDto, Order> findByUserAndStatus(
            @PathVariable("userId") Long userId,
            @RequestParam("status") OrderStatus status,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction direction) {
        return OrderEntityConverter.INS.fromPage(
                orderService.findByUser(userId, status,
                        PageRequest.of(page, size, Sort.by(direction, "createdDate")))
        );
    }

    @DeleteMapping("/order/{id}")
    public Boolean cancelOrder(@PathVariable("id") Long orderId) {
        orderService.cancelOrder(orderId);
        return true;
    }


}
