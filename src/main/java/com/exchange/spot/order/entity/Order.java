package com.exchange.spot.order.entity;

import com.exchange.spot.common.GenericEntity;
import com.exchange.spot.pair.Pair;
import com.exchange.spot.user.entity.User;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "order_entry")
@NoArgsConstructor
public class Order extends GenericEntity<Long> {
    @ManyToOne
    @JoinColumn(updatable = false)
    private Pair pair;
    @Enumerated(EnumType.STRING)
    @Column(updatable = false)
    private OrderType type;
    @ManyToOne
    @JoinColumn(updatable = false)
    private User createdBy;
    @Enumerated(EnumType.STRING)
    @Column(updatable = false)
    private OrderDirection direction;
    @Enumerated(EnumType.STRING)
    private OrderStatus status = OrderStatus.OPEN;
    @Column(updatable = false)
    private BigDecimal quantity;
    @Column(updatable = false)
    private BigDecimal price;

    @OneToMany(mappedBy = "order")
    List<OrderFillLog> fillLog;


    public Order(Long orderId) {
        super.setId(orderId);
    }
}
