package com.exchange.spot.order.entity;

public enum OrderDirection {
    ASK,
    BID;

    public boolean isAsk() {
        return this.equals(ASK);
    }

    public Long desiredAsset(Long asset1, Long asset2) {
        return isAsk() ? asset2 : asset1;
    }

    public Long providedAsset(Long asset1, Long asset2) {
        return isAsk() ? asset1 : asset2;
    }
}
