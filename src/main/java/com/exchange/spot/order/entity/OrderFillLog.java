package com.exchange.spot.order.entity;

import com.exchange.spot.common.GenericEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class OrderFillLog extends GenericEntity<Long> {
    @ManyToOne
    @JoinColumn(nullable = false,updatable = false)
    private Order order;
    private BigDecimal amount;
}
