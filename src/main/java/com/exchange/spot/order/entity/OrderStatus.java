package com.exchange.spot.order.entity;

public enum OrderStatus {
    QUEUED, OPEN, PARTIALLY_FILLED, FILLED, CANCELLED;

    public boolean filled() {
        return this.equals(FILLED);
    }
}
