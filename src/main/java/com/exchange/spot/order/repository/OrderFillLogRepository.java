package com.exchange.spot.order.repository;

import com.exchange.spot.common.GenericRepository;
import com.exchange.spot.order.entity.Order;
import com.exchange.spot.order.entity.OrderFillLog;

import java.util.List;

public interface OrderFillLogRepository extends GenericRepository<OrderFillLog,Long> {
    List<OrderFillLog> getByOrder(Order order);
}
