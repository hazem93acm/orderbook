package com.exchange.spot.order.repository;

import com.exchange.spot.common.GenericRepository;
import com.exchange.spot.order.entity.Order;
import com.exchange.spot.order.entity.OrderStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends GenericRepository<Order, Long> {
    @Query("SELECT P FROM Order P LEFT JOIN FETCH P.fillLog where P.id=:id")
    Optional<Order> getById(@Param("id") Long id);

    @Modifying
    @Query("UPDATE Order set status=:status WHERE id=:orderId")
    void update(@Param("orderId") Long orderId,@Param("status") OrderStatus status);

    @Query("SELECT O from Order O where " +
            "O.createdBy.id = :userId AND (:status is null OR O.status=:status)")
    Page<Order> findByUser(@Param("userId") Long userId, @Param("status") OrderStatus status, Pageable page);
}
