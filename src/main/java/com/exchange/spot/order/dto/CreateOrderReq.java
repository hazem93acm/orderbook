package com.exchange.spot.order.dto;

import com.exchange.spot.order.entity.OrderDirection;
import com.exchange.spot.order.entity.OrderType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateOrderReq {
    private String pair;
    private Long user;
    private BigDecimal quantity;
    private OrderDirection direction;
    private BigDecimal price;
    @NotNull
    private OrderType type;
}
