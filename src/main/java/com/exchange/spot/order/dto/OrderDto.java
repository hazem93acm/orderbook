package com.exchange.spot.order.dto;

import com.exchange.spot.order.entity.OrderDirection;
import com.exchange.spot.order.entity.OrderStatus;
import com.exchange.spot.order.entity.OrderType;
import com.exchange.spot.pair.dto.PairDto;
import com.exchange.spot.user.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDto {
    private Long id;
    private PairDto pair;
    private OrderType type;
    private UserDto createdBy;
    private OrderDirection direction;
    private OrderStatus status;
    private BigDecimal quantity;
    private BigDecimal price;
    private List<OrderFillLogDto> fillLog;
}
