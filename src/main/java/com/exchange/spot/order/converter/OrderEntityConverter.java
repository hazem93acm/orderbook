package com.exchange.spot.order.converter;

import com.exchange.spot.common.IEntityConverter;
import com.exchange.spot.order.dto.OrderDto;
import com.exchange.spot.order.entity.Order;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = OrderFillLogEntityConverter.class)
public interface OrderEntityConverter extends IEntityConverter<OrderDto, Order> {
    OrderEntityConverter INS = Mappers.getMapper(OrderEntityConverter.class);
}
