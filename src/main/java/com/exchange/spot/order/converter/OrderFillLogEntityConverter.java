package com.exchange.spot.order.converter;

import com.exchange.spot.common.IEntityConverter;
import com.exchange.spot.order.dto.OrderFillLogDto;
import com.exchange.spot.order.entity.OrderFillLog;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderFillLogEntityConverter extends IEntityConverter<OrderFillLogDto, OrderFillLog> {
    OrderFillLogEntityConverter INS = Mappers.getMapper(OrderFillLogEntityConverter.class);
}
