package com.exchange.spot.order.srevice;

import com.exchange.spot.engin.service.IOrderBookEventHadlerService;
import com.exchange.spot.order.entity.OrderStatus;
import com.exchange.spot.order.repository.OrderRepository;
import com.exchange.spot.orderbook.core.OrderEntry;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderServiceListener implements IOrderBookEventHadlerService {
    private final OrderRepository orderRepository;
    private final OrderFillLogService orderFillLogService;

    public void updateOrder(Long orderId, OrderStatus status) {
        orderRepository.update(orderId, status);
    }

    @Override
    @Transactional
    @Async
    public void handelOrderPlaced(OrderEntry order) {
        updateOrder(order.getId(), order.getOrderStatus());
    }

    @Override
    @Transactional
    @Async
    public void handelOrderFilled(OrderEntry order) {
        updateOrder(order.getId(), order.getOrderStatus());
        orderFillLogService.addToLog(order.getId(), order.getProcessedAmount());
    }

    @Override
    @Transactional
    @Async
    public void handelOrderCancelled(OrderEntry order) {
        updateOrder(order.getId(), order.getOrderStatus());
    }
}
