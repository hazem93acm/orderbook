package com.exchange.spot.order.srevice;

import com.exchange.spot.common.exception.ValidationException;
import com.exchange.spot.order.dto.CreateOrderReq;
import com.exchange.spot.order.entity.Order;

public interface IOrderParser<T extends CreateOrderReq> {
    Order parse(T createOrderDto) throws ValidationException;
}
