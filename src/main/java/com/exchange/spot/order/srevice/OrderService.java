package com.exchange.spot.order.srevice;

import com.exchange.spot.order.dto.CreateOrderReq;
import com.exchange.spot.order.entity.Order;
import com.exchange.spot.order.entity.OrderStatus;
import com.exchange.spot.order.repository.OrderRepository;
import com.exchange.spot.orderbook.service.OrderBookService;
import com.exchange.spot.wallet.service.OrderSpendBalanceService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import jakarta.xml.bind.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    @Lazy
    private final OrderBookService orderBookService;
    private final OrderParserService orderParserService;
    private final OrderSpendBalanceService spendBalanceService;

    @Transactional
    public Order createOrder(CreateOrderReq orderReq) throws IllegalAccessException, ValidationException {
        var order = orderParserService.parse(orderReq);
        var asset = order.getDirection().providedAsset(order.getPair().getAsset1().getId(), order.getPair().getAsset2().getId());
        order = orderRepository.save(order);
        spendBalanceService.spend(order.getCreatedBy().getId(), asset, order.getId(), order.getQuantity());
        orderBookService.addOrder(order);
        return order;
    }

    @Transactional
    public void cancelOrder(Long order) {
        orderBookService.cancelOrder(getById(order));
    }

    public Order getById(Long id) {
        return orderRepository.getById(id).orElseThrow(() -> new EntityNotFoundException("no order found with the given id"));
    }

    public Page<Order> findByUser(Long userId, OrderStatus status, Pageable pageable) {
        return orderRepository.findByUser(userId, status, pageable);
    }
}
