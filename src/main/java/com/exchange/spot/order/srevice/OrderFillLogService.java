package com.exchange.spot.order.srevice;

import com.exchange.spot.order.entity.Order;
import com.exchange.spot.order.entity.OrderFillLog;
import com.exchange.spot.order.repository.OrderFillLogRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderFillLogService {
    private final OrderFillLogRepository orderFillLogRepository;

    @Transactional
    public void addToLog(Long orderId, BigDecimal amount) {
        OrderFillLog orderFillLog = new OrderFillLog();
        orderFillLog.setOrder(new Order(orderId));
        orderFillLog.setAmount(amount);
        orderFillLogRepository.save(orderFillLog);
    }

    public List<OrderFillLog> getOrderLog(Long orderId){
        return orderFillLogRepository.getByOrder(new Order(orderId));
    }
}
