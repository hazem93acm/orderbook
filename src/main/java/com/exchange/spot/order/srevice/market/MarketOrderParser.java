package com.exchange.spot.order.srevice.market;

import com.exchange.spot.common.exception.ValidationException;
import com.exchange.spot.order.dto.CreateOrderReq;
import com.exchange.spot.order.entity.Order;
import com.exchange.spot.order.entity.OrderType;
import com.exchange.spot.order.srevice.BaseOrderParser;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class MarketOrderParser extends BaseOrderParser<CreateOrderReq> {
    @Override
    public Order parse(CreateOrderReq createOrderDto) throws ValidationException {
        var order = super.parse(createOrderDto);
        if (order.getDirection().isAsk()) {
            order.setPrice(new BigDecimal(Long.MAX_VALUE));
        } else {
            order.setPrice(new BigDecimal(Long.MIN_VALUE));
        }
        order.setType(OrderType.MARKET);
        return order;
    }
}
