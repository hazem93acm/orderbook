package com.exchange.spot.order.srevice.limit;

import com.exchange.spot.common.exception.ValidationException;
import com.exchange.spot.order.dto.CreateOrderReq;
import com.exchange.spot.order.entity.Order;
import com.exchange.spot.order.entity.OrderType;
import com.exchange.spot.order.srevice.BaseOrderParser;
import org.springframework.stereotype.Service;

@Service
public class LimitOrderParser extends BaseOrderParser<CreateOrderReq> {
    @Override
    public Order parse(CreateOrderReq createOrderDto) throws ValidationException {
        var order = super.parse(createOrderDto);
        if (createOrderDto.getPrice() == null) {
            throw new ValidationException("price not presented for limit order");
        }
        order.setPrice(createOrderDto.getPrice());
        order.setType(OrderType.LIMIT);
        return order;
    }
}
