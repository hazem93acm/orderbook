package com.exchange.spot.order.srevice;

import com.exchange.spot.common.exception.ValidationException;
import com.exchange.spot.order.dto.CreateOrderReq;
import com.exchange.spot.order.entity.Order;
import com.exchange.spot.order.entity.OrderStatus;
import com.exchange.spot.pair.PairService;
import com.exchange.spot.user.service.UserService;
import com.exchange.spot.wallet.service.OrderSpendBalanceService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;


@Setter
public class BaseOrderParser<T extends CreateOrderReq> implements IOrderParser<T> {
    @Autowired
    private OrderSpendBalanceService spendBalanceService;
    @Autowired
    private PairService pairService;
    @Autowired
    private UserService userService;

    public Order parse(T createOrderDto) throws ValidationException {
        if (createOrderDto == null) {
            throw new ValidationException("order can't be null");
        }
        if (createOrderDto.getDirection() == null) {
            throw new ValidationException("order direction can't be null");
        }
        if (createOrderDto.getQuantity() == null) {
            throw new ValidationException("order quantity can't be null");
        }
        if (createOrderDto.getQuantity().compareTo(BigDecimal.ZERO) <= 0) {
            throw new ValidationException("order quantity should be greater than 0");
        }
        if (createOrderDto.getPair() == null || createOrderDto.getPair().isBlank()) {
            throw new ValidationException("order pair should be specified");
        }
        if (createOrderDto.getUser() == null) {
            throw new ValidationException("order user should be specified");
        }
        var user = userService.getById(createOrderDto.getUser());
        var pair = pairService.getByName(createOrderDto.getPair());
        var asset = createOrderDto.getDirection().providedAsset(pair.getAsset1().getId(), pair.getAsset2().getId());
        spendBalanceService.isValidToSpend(user.getId(), asset
                , createOrderDto.getQuantity());
        Order order = new Order();
        order.setDirection(createOrderDto.getDirection());
        order.setCreatedBy(user);
        order.setPair(pair);
        order.setStatus(OrderStatus.OPEN);
        order.setQuantity(createOrderDto.getQuantity());
        return order;
    }
}
