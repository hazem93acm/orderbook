package com.exchange.spot.order.srevice;

import com.exchange.spot.order.dto.CreateOrderReq;
import com.exchange.spot.order.entity.Order;
import com.exchange.spot.order.srevice.limit.LimitOrderParser;
import com.exchange.spot.order.srevice.market.MarketOrderParser;
import jakarta.xml.bind.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class OrderParserService {
    private final MarketOrderParser marketOrderParser;
    private final LimitOrderParser limitOrderParser;

    public Order parse(CreateOrderReq createOrderDto) throws ValidationException {
        return switch (createOrderDto.getType()) {
            case LIMIT -> limitOrderParser.parse(createOrderDto);
            case MARKET -> marketOrderParser.parse(createOrderDto);
        };
    }
}
