package com.exchange.spot.asset;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AssetService {
    private final AssertRepository assertRepository;

    @Transactional
    public Asset createAsset(String name) {
        Asset asset = new Asset();
        asset.setName(name);
        return assertRepository.save(asset);
    }

    public Asset getByName(String name) {
        return assertRepository.getByName(name)
                .orElseThrow(() ->
                        new EntityNotFoundException(String.format("no asset with name %s found",name)));
    }
}
