package com.exchange.spot.asset;

import com.exchange.spot.common.GenericEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Asset extends GenericEntity<Long> {
    @Column(nullable = false, unique = true)
    private String name;

    public Asset(Long assetId) {
        this.setId(assetId);
    }
}
