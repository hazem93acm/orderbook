package com.exchange.spot.asset;

import com.exchange.spot.common.GenericRepository;

import java.util.Optional;

public interface AssertRepository extends GenericRepository<Asset, Long> {
    Optional<Asset> getByName(String name);
}
