package com.exchange.spot.asset;

import com.exchange.spot.asset.dto.AssetDto;
import com.exchange.spot.common.IEntityConverter;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AssetEntityConverter extends IEntityConverter<AssetDto, Asset> {
    AssetEntityConverter INST = Mappers.getMapper(AssetEntityConverter.class);
}
