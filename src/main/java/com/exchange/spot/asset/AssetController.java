package com.exchange.spot.asset;

import com.exchange.spot.asset.dto.AssetDto;
import com.exchange.spot.asset.dto.CreateAssetReq;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/asset")
@RequiredArgsConstructor
public class AssetController {

    private final AssetService assetService;

    @PostMapping
    public AssetDto create(@RequestBody CreateAssetReq createAssetReq) {
        return AssetEntityConverter.INST.fromEntity(assetService.createAsset(createAssetReq.getName()));
    }
}
