package com.exchange.spot.orderbook.event;

import com.exchange.spot.orderbook.core.OrderEntry;

public interface IOrderBookEventNotifier {
    void notifyOrderPlaced(OrderEntry order);

    void notifyOrderUpdated(OrderEntry order);

    void notifyOrderDeleted(OrderEntry order);
}
