package com.exchange.spot.orderbook.service;

import com.exchange.spot.engin.service.OrderBookEventsHandler;
import com.exchange.spot.order.entity.Order;
import com.exchange.spot.orderbook.core.OrderBook;
import com.exchange.spot.orderbook.core.OrderEntry;
import com.exchange.spot.pair.Pair;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;

@Service
@RequiredArgsConstructor
public class OrderBookService {
    @Lazy
    private final OrderBookEventsHandler orderBookEventsHandler;
    private final static ConcurrentHashMap<String, OrderBook> ORDER_BOOKS = new ConcurrentHashMap<>();

    public void createOrderBook(Pair pair) {
        ORDER_BOOKS.put(pair.getName(), new OrderBook(orderBookEventsHandler));
    }

    public OrderEntry addOrder(Order order) throws IllegalAccessException {
        if (!ORDER_BOOKS.containsKey(order.getPair().getName())) {
            createOrderBook(order.getPair());
        }
        return ORDER_BOOKS.get(order.getPair().getName()).addOrder(OrderEntry.fromOrder(order));
    }

    public void cancelOrder(Order order) {
        if (!ORDER_BOOKS.containsKey(order.getPair().getName())) {
            createOrderBook(order.getPair());
        }
        ORDER_BOOKS.get(order.getPair().getName()).cancelOrder(OrderEntry.fromOrder(order));
    }
}
