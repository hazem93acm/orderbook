package com.exchange.spot.orderbook.core;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Comparator;

@RequiredArgsConstructor
@Getter
public enum Strategies {
    FIFO((Comparator.comparing(OrderEntry::getTimestamp)));
    private final Comparator<OrderEntry> strategy;
}
