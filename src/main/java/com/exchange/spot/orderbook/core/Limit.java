package com.exchange.spot.orderbook.core;

import com.exchange.spot.engin.service.IOrderBookEventListener;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class Limit implements IOrderBookEventListener {
    private final BigDecimal price;
    private final LimitTable limitTable;
    private BigDecimal volume = BigDecimal.ZERO;
    private Long size = 0L;
    private OrderQueue orders;


    Limit(BigDecimal price, LimitTable limitTable) {
        this.price = price;
        this.limitTable = limitTable;
        orders = new OrderQueue(this);
    }

    OrderEntry head() {
        return size > 0 ? orders.getOrders().first() : null;
    }

    OrderEntry addOrder(OrderEntry order) {
        assert order.getPrice().equals(price);
        OrderEntry orderEntry = orders.addOrder(order);
        size = orders.getSize();
        volume = orders.getVolume();
        return orderEntry;
    }

    void deleteOrder(Long orderId) {
        orders.cancelOrder(orderId);
        size = orders.getSize();
        volume = orders.getVolume();
    }


    OrderEntry fillOrder(OrderEntry order) {
        var filledOrder = orders.fillOrder(order);
        size = orders.getSize();
        volume = orders.getVolume();
        return filledOrder;
    }

    @Override
    public void handleOrderPlacedEvent(OrderEntry order) {
        limitTable.handleOrderPlacedEvent(order);
    }

    @Override
    public void handleOrderFilledEvent(OrderEntry order) {
        limitTable.handleOrderFilledEvent(order);
    }

    @Override
    public void handleOrderDeletedEvent(OrderEntry order) {
        limitTable.handleOrderDeletedEvent(order);
    }

    void updateOrder(OrderEntry filledOrder) {
        orders.updateOrder(filledOrder);
        size = orders.getSize();
        volume = orders.getVolume();
    }
}
