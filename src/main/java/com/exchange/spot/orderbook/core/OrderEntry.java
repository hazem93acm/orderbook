package com.exchange.spot.orderbook.core;

import com.exchange.spot.order.entity.Order;
import com.exchange.spot.order.entity.OrderDirection;
import com.exchange.spot.order.entity.OrderStatus;
import com.exchange.spot.order.entity.OrderType;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

@Getter
@Builder(toBuilder = true)
public class OrderEntry {
    private final Long id;
    private final BigDecimal price;
    private final OrderDirection direction;
    private BigDecimal quantity;
    private BigDecimal processedAmount;
    private final Long user;
    private final Instant timestamp;
    private final OrderType type;
    private final Long desiredAsset;
    private final Long providedAsset;
    private final String pair;
    @Builder.Default
    private OrderStatus orderStatus = OrderStatus.OPEN;

    public OrderEntry clone() {
        return this.toBuilder().build();
    }

    public static OrderEntry fromOrder(Order order) {
        return OrderEntry.builder()
                .user(order.getCreatedBy().getId())
                .price(order.getPrice())
                .direction(order.getDirection())
                .quantity(order.getQuantity())
                .id(order.getId())
                .timestamp(order.getCreatedDate())
                .type(order.getType())
                .desiredAsset(order.getDirection().desiredAsset(order.getPair().getAsset1().getId(), order.getPair().getAsset2().getId()))
                .providedAsset(order.getDirection().providedAsset(order.getPair().getAsset1().getId(), order.getPair().getAsset2().getId()))
                .pair(order.getPair().getName())
                .orderStatus(order.getStatus())
                .processedAmount(BigDecimal.ZERO)
                .build();
    }

    void update(BigDecimal delta) {
        assert quantity.compareTo(delta) >= 0;
        quantity = quantity.subtract(delta);
        processedAmount = delta;
        if (quantity.compareTo(BigDecimal.ZERO) == 0) {
            updateStatus(OrderStatus.FILLED);
        } else {
            updateStatus(OrderStatus.PARTIALLY_FILLED);
        }
    }

    void copyValues(OrderEntry orderEntry) {
        quantity = orderEntry.getQuantity();
        processedAmount = orderEntry.getProcessedAmount();
        orderStatus = orderEntry.getOrderStatus();
    }

    void updateStatus(OrderStatus status) {
        this.orderStatus = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderEntry that = (OrderEntry) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
