package com.exchange.spot.orderbook.core;

import com.exchange.spot.engin.service.IOrderBookEventListener;
import com.exchange.spot.order.entity.OrderDirection;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

@Getter
public class LimitTable implements IOrderBookEventListener {
    private final TreeMap<BigDecimal, Limit> limits;
    private BigDecimal volume;
    private final OrderDirection direction;

    private final IOrderBookEventListener listener;

    LimitTable(OrderDirection direction, IOrderBookEventListener listener) {
        this.direction = direction;
        Comparator<BigDecimal> comparator;
        if (OrderDirection.ASK.equals(direction)) {
            comparator = BigDecimal::compareTo;
        } else
            comparator = Comparator.reverseOrder();
        limits = new TreeMap<>(comparator);
        this.listener = listener;
    }

    OrderEntry addOrder(OrderEntry order) {
        Limit priceLimit;
        if (limits.containsKey(order.getPrice())) {
            priceLimit = limits.get(order.getPrice());
        } else {
            priceLimit = new Limit(order.getPrice(), this);
            limits.put(order.getPrice(), priceLimit);
        }
        OrderEntry orderEntry = priceLimit.addOrder(order);
        volume = priceLimit.getVolume();
        return orderEntry;
    }

    private void deleteLimit(BigDecimal price) {
        limits.remove(price);
    }

    void deleteOrder(OrderEntry order) {
        assert limits.containsKey(order.getPrice());
        var priceLimit = limits.get(order.getPrice());
        priceLimit.deleteOrder(order.getId());
        volume = priceLimit.getVolume();
        if (priceLimit.getSize() == 0) {
            deleteLimit(order.getPrice());
        }
    }

    Map.Entry<BigDecimal, Limit> bestPrice() {
        return limits.size() > 0 ? limits.firstEntry() : null;
    }

    boolean hasPrice(BigDecimal price) {
        return direction.isAsk() ? limits.ceilingKey(price) != null : limits.floorKey(price) != null;
    }

    OrderEntry fill(OrderEntry order) throws IllegalAccessException {
        var bestPrice = bestPrice();
        if (bestPrice != null) {
            if (order.getDirection().equals(direction)) {
                throw new IllegalAccessException();
            }
            OrderEntry filledOrder = null;
            if ((direction.isAsk() && bestPrice.getKey().compareTo(order.getPrice()) >= 0)
                    || (!direction.isAsk() && bestPrice.getKey().compareTo(order.getPrice()) <= 0)) {
                filledOrder = bestPrice.getValue().fillOrder(order);
            }
            if (filledOrder != null) {
                if(bestPrice.getValue().getSize().equals(0L)){
                    deleteLimit(bestPrice.getKey());
                }
                if (!filledOrder.getOrderStatus().filled()) {
                    return fill(order);
                }
            }

        }
        return order;
    }

    @Override
    public void handleOrderPlacedEvent(OrderEntry order) {
        listener.handleOrderPlacedEvent(order);
    }

    @Override
    public void handleOrderFilledEvent(OrderEntry order) {
        listener.handleOrderFilledEvent(order);
    }

    @Override
    public void handleOrderDeletedEvent(OrderEntry order) {
        listener.handleOrderDeletedEvent(order);
    }

    void updateOrder(OrderEntry order) {
        var priceLimit = limits.get(order.getPrice());
        limits.get(order.getPrice()).updateOrder(order);
        volume = priceLimit.getVolume();
        if (priceLimit.getSize() == 0) {
            deleteLimit(order.getPrice());
        }
    }
}
