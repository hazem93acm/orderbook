package com.exchange.spot.orderbook.core;

import com.exchange.spot.engin.service.IOrderBookEventListener;
import com.exchange.spot.order.entity.OrderDirection;
import lombok.Getter;

@Getter
public final class OrderBook implements IOrderBookEventListener {
    private final LimitTable asks;
    private final LimitTable bids;

    private IOrderBookEventListener eventsHandler;

    public OrderBook(IOrderBookEventListener eventsHandler) {
        this.asks = new LimitTable(OrderDirection.ASK, this);
        this.bids = new LimitTable(OrderDirection.BID, this);
        this.eventsHandler = eventsHandler;
    }

    public OrderEntry addOrder(OrderEntry order) throws IllegalAccessException {
        LimitTable limitTable = getByDirection(order.getDirection());
        var orderEntry = limitTable.addOrder(order);
        matchOrder(order.getDirection());
        return orderEntry;
    }


    public void cancelOrder(OrderEntry order) {
        LimitTable limitTable = getByDirection(order.getDirection());
        limitTable.deleteOrder(order);
    }

    private void matchOrder(OrderDirection direction) throws IllegalAccessException {
        var bidBestPrice = bids.bestPrice();
        var askBestPrice = asks.bestPrice();
        if (askBestPrice != null && bidBestPrice != null) {
            var filledOrder = getByOrderFill(direction).fill(getByDirection(direction).bestPrice().getValue().head());
            updateOrder(filledOrder);
        }
    }

    private void updateOrder(OrderEntry filledOrder) {
        getByDirection(filledOrder.getDirection()).updateOrder(filledOrder);
    }

    private LimitTable getByDirection(OrderDirection direction) {
        return direction.isAsk() ? asks : bids;
    }

    private LimitTable getByOrderFill(OrderDirection direction) {
        return direction.isAsk() ? bids : asks;
    }

    @Override
    public void handleOrderPlacedEvent(OrderEntry order) {
        eventsHandler.handleOrderPlacedEvent(order);
    }

    @Override
    public void handleOrderFilledEvent(OrderEntry order) {
        eventsHandler.handleOrderFilledEvent(order);
    }

    @Override
    public void handleOrderDeletedEvent(OrderEntry order) {
        eventsHandler.handleOrderDeletedEvent(order);
    }
}
