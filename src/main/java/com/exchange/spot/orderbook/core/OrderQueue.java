package com.exchange.spot.orderbook.core;

import com.exchange.spot.order.entity.OrderStatus;
import com.exchange.spot.orderbook.event.IOrderBookEventNotifier;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.*;

@Getter
public class OrderQueue implements IOrderBookEventNotifier {
    private final TreeSet<OrderEntry> orders;
    private final Map<Long, OrderEntry> orderMap;
    private Long size = 0L;
    private BigDecimal volume = BigDecimal.ZERO;
    private final Limit limit;

    public OrderQueue(Limit limit) {
        this.limit = limit;
        this.orders = new TreeSet<>(Strategies.FIFO.getStrategy());
        orderMap = new HashMap<>();
    }

    synchronized OrderEntry addOrder(OrderEntry order) {
        assert !orderMap.containsKey(order.getId());
        var mutatedOrder = order.clone();
        mutatedOrder.updateStatus(OrderStatus.QUEUED);
        orders.add(mutatedOrder);
        size += 1;
        volume = volume.add(mutatedOrder.getQuantity());
        orderMap.put(mutatedOrder.getId(), mutatedOrder);
        notifyOrderPlaced(mutatedOrder);
        return order;
    }

    synchronized void cancelOrder(Long orderId) {
        assert orderMap.containsKey(orderId);
        var order = orderMap.get(orderId);
        order.updateStatus(OrderStatus.CANCELLED);
        orders.remove(order);
        size -= 1;
        volume = volume.subtract(order.getQuantity());
        orderMap.remove(order.getId());
        notifyOrderDeleted(order);
    }

    private void closeOrder(Long orderId) {
        assert orderMap.containsKey(orderId);
        var order = orderMap.get(orderId);
        orders.remove(order);
        size -= 1;
        volume = volume.subtract(order.getQuantity());
        orderMap.remove(order.getId());
    }

    private synchronized void process(Long orderId, BigDecimal amount) {
        var order = orderMap.get(orderId);
        order.update(amount);
        if (OrderStatus.FILLED.equals(order.getOrderStatus())) {
            closeOrder(orderId);
        } else {
            volume = volume.subtract(order.getQuantity());
        }
        notifyOrderUpdated(order);
    }


    synchronized OrderEntry fillOrder(OrderEntry order) {
        var itr = orders.iterator();
        boolean filled = false;
        Map<Long, BigDecimal> affectedOrders = new HashMap<>();
        while (itr.hasNext() && !filled) {
            var currentOrder = itr.next();
            if (currentOrder.getQuantity().compareTo(order.getQuantity()) >= 0) {
                affectedOrders.put(currentOrder.getId(), order.getQuantity());
                order.update(order.getQuantity());
                filled = true;
            } else {
                affectedOrders.put(currentOrder.getId(), currentOrder.getQuantity());
                order.update(currentOrder.getQuantity());
            }
        }
        affectedOrders.forEach(this::process);
        return order;
    }

    @Override
    public void notifyOrderPlaced(OrderEntry order) {
        limit.handleOrderPlacedEvent(order.clone());
    }

    @Override
    public void notifyOrderUpdated(OrderEntry order) {
        limit.handleOrderFilledEvent(order.clone());
    }

    @Override
    public void notifyOrderDeleted(OrderEntry order) {
        limit.handleOrderDeletedEvent(order.clone());
    }

    void updateOrder(OrderEntry filledOrder) {
        assert orderMap.containsKey(filledOrder.getId());
        var currentOrder = orders.first();
        assert currentOrder.equals(filledOrder);
        if (filledOrder.getOrderStatus().filled()) {
            closeOrder(filledOrder.getId());
        } else {
            currentOrder.copyValues(filledOrder);
            volume = volume.subtract(filledOrder.getQuantity());
            orderMap.get(filledOrder.getId()).copyValues(filledOrder);
        }
        notifyOrderUpdated(filledOrder);
    }
}
