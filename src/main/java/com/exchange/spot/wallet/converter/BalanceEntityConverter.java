package com.exchange.spot.wallet.converter;

import com.exchange.spot.common.IEntityConverter;
import com.exchange.spot.wallet.dto.BalanceDto;
import com.exchange.spot.wallet.entity.UserBalance;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BalanceEntityConverter extends IEntityConverter<BalanceDto, UserBalance> {
    BalanceEntityConverter INST = Mappers.getMapper(BalanceEntityConverter.class);
}
