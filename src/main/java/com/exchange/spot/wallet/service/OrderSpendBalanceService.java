package com.exchange.spot.wallet.service;


import com.exchange.spot.common.exception.ValidationException;

import java.math.BigDecimal;

public interface OrderSpendBalanceService {
    boolean isValidToSpend(Long userId, Long assetId, BigDecimal amount) throws ValidationException;

    void spend(Long userId, Long assetId,Long orderId, BigDecimal amount) throws ValidationException;
}
