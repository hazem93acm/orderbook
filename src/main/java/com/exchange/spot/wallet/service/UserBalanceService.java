package com.exchange.spot.wallet.service;

import com.exchange.spot.asset.Asset;
import com.exchange.spot.common.exception.ValidationException;
import com.exchange.spot.user.entity.User;
import com.exchange.spot.wallet.entity.UserBalance;
import com.exchange.spot.wallet.repository.TransactionType;
import com.exchange.spot.wallet.repository.UserBalanceRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserBalanceService implements OrderSpendBalanceService {
    private final UserBalanceRepository userBalanceRepository;
    private final WalletTransactionService walletTransactionService;

    public List<UserBalance> getUserBalances(Long userId) {
        return userBalanceRepository.getByUser(userId);
    }

    public UserBalance getUserBalance(Long userId, Long assetId) {
        return userBalanceRepository.getByUserAndAsset(userId, assetId);
    }

    @Transactional
    public void deposit(Long userId, Long assetId, BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException();
        }
        var assetBalance = getUserBalance(userId, assetId);
        if (assetBalance == null) {
            userBalanceRepository.save(
                    UserBalance.builder()
                            .balance(amount)
                            .user(new User(userId))
                            .asset(new Asset(assetId))
                            .build()
            );
            walletTransactionService.createTransaction(userId, assetId, amount, TransactionType.FUND);
            return;
        }
        walletTransactionService.createTransaction(userId, assetId, amount, TransactionType.FUND);
        userBalanceRepository.deposit(userId, assetId, amount);
    }

    public void addOrderExecutionFunds(Long userId, Long orderId, Long assetId, BigDecimal amount,TransactionType transactionType) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException();
        }
        walletTransactionService.createTransaction(userId, assetId, orderId, amount, transactionType);
        userBalanceRepository.deposit(userId, assetId, amount);
    }

    @Transactional
    private void deduct(Long userId, Long assetId, BigDecimal amount) {
        assert amount.compareTo(BigDecimal.ZERO) > 0;
        var assetBalance = getUserBalance(userId, assetId);
        if (assetBalance == null || amount.compareTo(assetBalance.getBalance()) > 0) {
            throw new IllegalArgumentException();
        }
        userBalanceRepository.deduct(userId, assetId, amount);
    }

    @Override
    public boolean isValidToSpend(Long userId, Long assetId, BigDecimal amount) throws ValidationException {
        var balance = getUserBalance(userId, assetId);
        if (balance == null)
            throw new ValidationException("not enough balance");
        var res = balance.getBalance().compareTo(amount) >= 0;
        if (!res) {
            throw new ValidationException("not enough balance");
        }
        return true;
    }

    @Override
    public void spend(Long userId, Long assetId, Long orderId, BigDecimal amount) throws ValidationException {
        if (isValidToSpend(userId, assetId, amount)) {
            deduct(userId, assetId, amount);
            walletTransactionService.createTransaction(userId, assetId, orderId, amount.negate(), TransactionType.PLACE_ORDER);
        }
    }

}
