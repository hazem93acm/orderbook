package com.exchange.spot.wallet.service;

import com.exchange.spot.engin.service.IOrderBookEventHadlerService;
import com.exchange.spot.fee.FeeService;
import com.exchange.spot.orderbook.core.OrderEntry;
import com.exchange.spot.treasury.TreasuryService;
import com.exchange.spot.wallet.repository.TransactionType;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class WalletOrderBookEventHandlerService implements IOrderBookEventHadlerService {

    private final UserBalanceService userBalanceService;
    private final WalletTransactionService walletTransactionService;

    private final FeeService feeService;
    private final TreasuryService treasuryService;

    @Override
    @SneakyThrows
    public void handelOrderPlaced(OrderEntry order) {
        var tx = walletTransactionService.getByOrderId(order.getId());
        if (tx == null || !tx.getAmount().abs().equals(order.getQuantity().abs())) {
            throw new IllegalAccessException("Order placed without deduction it from the wallet");
        }
    }

    @Override
    @SneakyThrows
    public void handelOrderFilled(OrderEntry order) {
        var fee = feeService.calculateFee(order.getType(), order.getProcessedAmount(), order.getUser());
        var amount = order.getProcessedAmount();
        if (fee.compareTo(BigDecimal.ZERO) > 0) {
            amount = amount.subtract(fee);
            treasuryService.add(order.getDesiredAsset(), fee);
            walletTransactionService.createTransaction(order.getUser(), order.getDesiredAsset(), order.getId(),
                    fee.negate(), TransactionType.FEE);
        }
        userBalanceService.addOrderExecutionFunds(order.getUser(),
                order.getId(),
                order.getDesiredAsset(),
                amount,
                TransactionType.ORDER_FILLED
        );
    }

    @Override
    public void handelOrderCancelled(OrderEntry order) {
        userBalanceService.addOrderExecutionFunds(order.getUser(),
                order.getId(),
                order.getProvidedAsset(),
                order.getQuantity(),
                TransactionType.CANCEL_ORDER
        );
    }
}
