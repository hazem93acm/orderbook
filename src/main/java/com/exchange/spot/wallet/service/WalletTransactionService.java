package com.exchange.spot.wallet.service;

import com.exchange.spot.asset.Asset;
import com.exchange.spot.order.entity.Order;
import com.exchange.spot.user.entity.User;
import com.exchange.spot.wallet.entity.WalletTransaction;
import com.exchange.spot.wallet.repository.TransactionType;
import com.exchange.spot.wallet.repository.WalletTransactionRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class WalletTransactionService {
    private final WalletTransactionRepository walletTransactionRepository;

    @Transactional
    public WalletTransaction createTransaction(Long userId, Long assetId, Long orderId,
                                               BigDecimal amount, TransactionType transactionType) {
        WalletTransaction walletTransaction = new WalletTransaction();
        walletTransaction.setUser(new User(userId));
        walletTransaction.setAsset(new Asset(assetId));
        if (orderId != null)
            walletTransaction.setReferencedOrder(new Order(orderId));
        walletTransaction.setAmount(amount);
        walletTransaction.setType(transactionType);
        return walletTransactionRepository.save(walletTransaction);
    }

    @Transactional
    public WalletTransaction createTransaction(Long userId, Long assetId,
                                               BigDecimal amount, TransactionType transactionType) {
        return createTransaction(userId, assetId, null, amount, transactionType);
    }

    public WalletTransaction getByOrderId(Long orderId) {
        return walletTransactionRepository.getByOrder(orderId);
    }
}
