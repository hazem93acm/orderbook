package com.exchange.spot.wallet.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FundWalletReq {
    private Long asset;
    private Long user;
    private BigDecimal amount;
}
