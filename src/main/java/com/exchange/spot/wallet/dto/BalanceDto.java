package com.exchange.spot.wallet.dto;

import com.exchange.spot.asset.dto.AssetDto;
import com.exchange.spot.user.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class BalanceDto {
    @JsonIgnore
    private UserDto user;
    private AssetDto asset;
    private BigDecimal balance;
}
