package com.exchange.spot.wallet.entity;

import com.exchange.spot.asset.Asset;
import com.exchange.spot.common.GenericEntity;
import com.exchange.spot.user.entity.User;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.*;

import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserBalance extends GenericEntity<Long> {
    @ManyToOne
    private User user;
    @ManyToOne
    private Asset asset;
    private BigDecimal balance;
}
