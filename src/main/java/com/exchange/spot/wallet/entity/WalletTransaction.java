package com.exchange.spot.wallet.entity;

import com.exchange.spot.asset.Asset;
import com.exchange.spot.common.GenericEntity;
import com.exchange.spot.order.entity.Order;
import com.exchange.spot.user.entity.User;
import com.exchange.spot.wallet.repository.TransactionType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class WalletTransaction extends GenericEntity<Long> {
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private User user;
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Asset asset;
    @ManyToOne
    @JoinColumn(updatable = false)
    private Order referencedOrder;
    @Column(nullable = false, updatable = false)
    private BigDecimal amount;
    @Enumerated(EnumType.STRING)
    private TransactionType type;
}
