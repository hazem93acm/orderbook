package com.exchange.spot.wallet.controller;

import com.exchange.spot.wallet.converter.BalanceEntityConverter;
import com.exchange.spot.wallet.service.UserBalanceService;
import com.exchange.spot.wallet.dto.BalanceDto;
import com.exchange.spot.wallet.dto.FundWalletReq;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/wallet")
@RequiredArgsConstructor
public class AssetBalanceController {
    private final UserBalanceService userBalanceService;

    @PostMapping("fund")
    public Boolean fund(@RequestBody FundWalletReq fundWalletReq) {
        userBalanceService.deposit(fundWalletReq.getUser(), fundWalletReq.getAsset(), fundWalletReq.getAmount());
        return true;
    }

    @GetMapping("user/{userId}/asset/{assetId}")
    public BalanceDto getByUserAndAsset(@PathVariable("userId") Long userId,
                                        @PathVariable("assetId") Long assetId) {
        return BalanceEntityConverter.INST.fromEntity(userBalanceService.getUserBalance(userId, assetId));
    }

    @GetMapping("user/{userId}")
    public List<BalanceDto> getByUser(@PathVariable("userId") Long userId) {
        return BalanceEntityConverter.INST.fromEntities(userBalanceService.getUserBalances(userId));
    }

}
