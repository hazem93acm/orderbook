package com.exchange.spot.wallet.repository;

import com.exchange.spot.common.GenericRepository;
import com.exchange.spot.wallet.entity.WalletTransaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WalletTransactionRepository extends GenericRepository<WalletTransaction, Long> {

    @Query("SELECT UB FROM WalletTransaction as UB WHERE UB.user.id=:userId")
    List<WalletTransaction> getByUser(@Param("userId") Long userId);

    @Query("SELECT UB FROM WalletTransaction as UB WHERE UB.user.id=:userId AND UB.asset.id =:asset")
    List<WalletTransaction> getByUserAndAsset(@Param("userId") Long userId, @Param("asset") Long asset);

    @Query("SELECT UB FROM WalletTransaction as UB WHERE UB.referencedOrder.id =:orderId")
    WalletTransaction getByOrder(@Param("orderId") Long orderId);


}
