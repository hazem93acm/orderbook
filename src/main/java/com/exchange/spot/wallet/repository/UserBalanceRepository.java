package com.exchange.spot.wallet.repository;

import com.exchange.spot.common.GenericRepository;
import com.exchange.spot.wallet.entity.UserBalance;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface UserBalanceRepository extends GenericRepository<UserBalance, Long> {

    @Query("SELECT UB FROM UserBalance as UB WHERE UB.user.id=:userId")
    List<UserBalance> getByUser(@Param("userId") Long userId);

    @Query("SELECT UB FROM UserBalance as UB WHERE UB.user.id=:userId AND UB.asset.id =:asset")
    UserBalance getByUserAndAsset(@Param("userId") Long userId, @Param("asset") Long asset);


    @Modifying
    @Query("UPDATE UserBalance set balance = balance+:amount WHERE user.id=:userId AND asset.id=:asset")
    int deposit(@Param("userId") Long userId,
                @Param("asset") Long asset,
                @Param("amount") BigDecimal amount);

    @Modifying
    @Query("UPDATE UserBalance set balance = balance-:amount WHERE user.id=:userId AND asset.id=:asset")
    int deduct(@Param("userId") Long userId,
               @Param("asset") Long asset,
               @Param("amount") BigDecimal amount);
}
