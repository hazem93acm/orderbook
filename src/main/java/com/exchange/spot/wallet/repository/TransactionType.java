package com.exchange.spot.wallet.repository;

public enum TransactionType {
    FUND, PLACE_ORDER, CANCEL_ORDER, WITHDRAW, ORDER_FILLED,FEE
}
