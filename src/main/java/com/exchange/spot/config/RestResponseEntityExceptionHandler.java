package com.exchange.spot.config;

import com.exchange.spot.common.ResponseModel;
import com.exchange.spot.common.exception.ValidationException;
import jakarta.persistence.EntityNotFoundException;
import org.hibernate.internal.util.ExceptionHelper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {EntityNotFoundException.class})
    protected ResponseEntity<Object> handleEntityNotFoundConflict(
            RuntimeException ex, WebRequest request) {
        String bodyOfResponse = ExceptionHelper.getRootCause(ex).getMessage();
        ResponseModel<?> errorResponse = ResponseModel.ofError(bodyOfResponse,
                HttpStatus.NOT_FOUND.value());
        return handleExceptionInternal(ex, errorResponse,
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {ValidationException.class})
    protected ResponseEntity<Object> handleValidationConflict(
            RuntimeException ex, WebRequest request) {
        String bodyOfResponse = ExceptionHelper.getRootCause(ex).getMessage();
        ResponseModel<?> errorResponse = ResponseModel.ofError(bodyOfResponse,
                HttpStatus.BAD_REQUEST.value());
        return handleExceptionInternal(ex, errorResponse,
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleConflict(
            RuntimeException ex, WebRequest request) {
        String bodyOfResponse = ExceptionHelper.getRootCause(ex).getMessage();
        ResponseModel<?> errorResponse = ResponseModel.ofError(bodyOfResponse,
                HttpStatus.INTERNAL_SERVER_ERROR.value());
        ex.printStackTrace();
        return handleExceptionInternal(ex, errorResponse,
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}