package com.exchange.spot.engin.service;

import com.exchange.spot.orderbook.core.OrderEntry;

public interface IOrderBookEventListener {
    void handleOrderPlacedEvent(OrderEntry order) ;

    void handleOrderFilledEvent(OrderEntry order) ;

    void handleOrderDeletedEvent(OrderEntry order) ;
}
