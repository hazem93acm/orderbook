package com.exchange.spot.engin.service;

import com.exchange.spot.orderbook.core.OrderEntry;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
public class OrderBookEventsHandler implements IOrderBookEventListener {
    private final List<IOrderBookEventHadlerService> listeners;

    public OrderBookEventsHandler(List<IOrderBookEventHadlerService> listeners) {
        this.listeners = listeners;
        listeners.sort(Comparator.comparing(IOrderBookEventHadlerService::priority));
    }

    @Transactional
    public void handleOrderPlacedEvent(OrderEntry order) {
        listeners.forEach(iOrderEventListenerService -> iOrderEventListenerService.handelOrderPlaced(order));
    }

    public void handleOrderFilledEvent(OrderEntry order) {
        listeners.forEach(iOrderEventListenerService -> iOrderEventListenerService.handelOrderFilled(order));
    }

    public void handleOrderDeletedEvent(OrderEntry order) {
        listeners.forEach(iOrderEventListenerService -> iOrderEventListenerService.handelOrderCancelled(order));
    }
}
