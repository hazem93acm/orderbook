package com.exchange.spot.engin.service;

import com.exchange.spot.orderbook.core.OrderEntry;

public interface IOrderBookEventHadlerService {
    default Integer priority() {
        return Integer.MIN_VALUE;
    }

    void handelOrderPlaced(OrderEntry order);
    void handelOrderFilled(OrderEntry order);
    void handelOrderCancelled(OrderEntry order);

}
