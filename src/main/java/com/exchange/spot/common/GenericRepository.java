package com.exchange.spot.common;

import org.springframework.data.repository.CrudRepository;

public interface GenericRepository<T extends GenericEntity<ID>, ID> extends CrudRepository<T, ID> {
}
