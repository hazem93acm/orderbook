package com.exchange.spot.common;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import java.time.Instant;

@MappedSuperclass
@Getter
@Setter
public abstract class GenericEntity<ID> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private ID id;
    @Column(name = "created_date", nullable = false, updatable = false)
    private Instant createdDate;
    @Column(name = "modified_date")
    private Instant modifiedDate;

    @PrePersist
    public void prePersist() {
        createdDate = Instant.now();
        modifiedDate = Instant.now();
    }

    @PreUpdate
    public void preUpdate() {
        modifiedDate = Instant.now();
    }
}
