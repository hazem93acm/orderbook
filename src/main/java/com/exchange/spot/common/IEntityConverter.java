package com.exchange.spot.common;

import org.hibernate.Hibernate;
import org.mapstruct.Condition;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface IEntityConverter<DTO, ENTITY extends GenericEntity<?>> {

    DTO fromEntity(ENTITY entity);

    ENTITY fromDTO(DTO dto);

    @Condition
    default boolean isInitialize(
            List<ENTITY> sourceCollection) {
        if (sourceCollection == null) {
            return true;
        }
        return Hibernate.isInitialized((sourceCollection));
    }

    default List<DTO> fromEntities(List<ENTITY> entities) {
        if (entities == null) {
            return null;
        }
        if (Hibernate.isInitialized(entities)) {
            return entities.stream()
                    .map(this::fromEntity).collect(Collectors.toList());
        }
        return null;
    }

    default PageableResponse<DTO, ENTITY> fromPage(Page<ENTITY> entities) {
        return new PageableResponse<>(entities, this);
    }

    default List<ENTITY> fromDTOs(List<DTO> dtos) {
        if (dtos == null) {
            return null;
        }
        return dtos.stream().map(this::fromDTO).collect(Collectors.toList());
    }
}
