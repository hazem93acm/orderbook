package com.exchange.spot.common;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.Collection;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PageableResponse<DTO, ENTITY extends GenericEntity<?>> {
    private Collection<DTO> data;
    private Long totalElements;
    private int totalPages;
    private int pageSize;

    public PageableResponse(Page<ENTITY> page, IEntityConverter<DTO, ENTITY> converter) {
        if (page != null) {
            this.data = converter.fromEntities(page.getContent());
            this.totalElements = page.getTotalElements();
            this.pageSize = page.getSize();
            this.totalPages = page.getTotalPages();
        }
    }
}
