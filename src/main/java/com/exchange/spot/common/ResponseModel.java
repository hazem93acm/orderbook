package com.exchange.spot.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseModel<T> {
    private T result;
    private ErrorModel error;

    public static <T> ResponseModel<T> ofResult(T result) {
        ResponseModel<T> model = new ResponseModel<>();
        model.setResult(result);
        return model;
    }

    public static ResponseModel<?> ofError(String message, int code) {
        ResponseModel<?> model = new ResponseModel<>();
        model.setError(new ErrorModel(code, message));
        return model;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ErrorModel {
        private int code;
        private String message;
    }
}
