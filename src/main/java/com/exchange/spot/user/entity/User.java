package com.exchange.spot.user.entity;

import com.exchange.spot.common.GenericEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import lombok.*;

@Entity
@Table(name = "app_user")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User extends GenericEntity<Long> {
    private String name;
    @Enumerated(EnumType.STRING)
    @Builder.Default
    private UserType userType = UserType.CLIENT;

    public User(Long id) {
        this.setId(id);
    }
}
