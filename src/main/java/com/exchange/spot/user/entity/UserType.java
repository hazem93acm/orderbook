package com.exchange.spot.user.entity;

public enum UserType {
    CLIENT, BOT, PLATFORM
}
