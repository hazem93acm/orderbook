package com.exchange.spot.user.controller;

import com.exchange.spot.user.converter.UserDataConverter;
import com.exchange.spot.user.dto.CreateUserReq;
import com.exchange.spot.user.dto.UserDto;
import com.exchange.spot.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping
    public UserDto create(@RequestBody CreateUserReq createUserReq) {
        var user = userService.createUser(createUserReq);
        return UserDataConverter.INST.fromEntity(user);
    }

    @GetMapping("{id}")
    public UserDto getById(@PathVariable("id") Long id) {
        var user = userService.getById(id);
        return UserDataConverter.INST.fromEntity(user);
    }
}
