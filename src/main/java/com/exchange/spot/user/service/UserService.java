package com.exchange.spot.user.service;

import com.exchange.spot.user.dto.CreateUserReq;
import com.exchange.spot.user.entity.User;
import com.exchange.spot.user.entity.UserType;
import com.exchange.spot.user.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    @Transactional
    User create(String userName, UserType userType) {
        return userRepository.save(User.builder()
                .name(userName)
                .userType(userType)
                .build());
    }

    public User getById(Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("no user with id %d found", userId)));
    }

    @Transactional
    public User createUser(CreateUserReq createUserReq) {
        return create(createUserReq.getName(), createUserReq.getUserType());
    }
}
