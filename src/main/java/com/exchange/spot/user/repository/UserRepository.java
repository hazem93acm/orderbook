package com.exchange.spot.user.repository;

import com.exchange.spot.common.GenericRepository;
import com.exchange.spot.user.entity.User;

public interface UserRepository extends GenericRepository<User, Long> {
}
