package com.exchange.spot.user.dto;

import com.exchange.spot.user.entity.UserType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateUserReq {
    private String name;
    private UserType userType = UserType.CLIENT;
}
