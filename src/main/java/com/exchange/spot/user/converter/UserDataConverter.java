package com.exchange.spot.user.converter;

import com.exchange.spot.common.IEntityConverter;
import com.exchange.spot.user.dto.UserDto;
import com.exchange.spot.user.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserDataConverter extends IEntityConverter<UserDto,User> {
    UserDataConverter INST = Mappers.getMapper(UserDataConverter.class);
}
