# Order Book
This project has been created as proof of concept, and it's not production ready!

## Main Modules
This project is spring boot project and divided based on the modules and functionality.
You can access all the available endpoint after running the app and open [swagger](http://localhost:8080/spot/swagger-ui/index.html).
### User
App user module with all services and entities and endpoints required to (create/get)user.
### Asset
To manage the platform asset, asset will be part of user wallet and pairs, it has all the assets supported by the platform.
### Wallet
To control the user wallets and all operation and keep track of all income and outcome transactions.
### Pair
To manage the available trading pairs in the platform.
### Fee
To manage the platform order and users discounts and calculate the fees for the filled orders.
### Treasury 
To manage the platform treasury and all the income from the deducted fees.
### OrderBook
In memory simple order book implementation with fast access to order and price limits and dynamic order queuing strategies, 
it works as events emitter to notify listeners about the changes in the order book (cancel/create/fill) orders.
### Order
To manage the order operation (create/fetch/query/cancel) it works with order book to append new orders to the order book of the given pair.
### Engine
For now, it works as event listener to the order book events and as service registry for all event handler services.
It manages the events and broadcast them to the handlers.
### Common
Common app classes and interfaces which being user by all app modules(exceptions,generics...).
### Config
All Application configuration.
### Test
Contains all app tests.

